##Author: AndresFWilT##
def mcd_I(n,m):##mcd iterativa##
    A = max(n, m)
    B = min(n, m)
    while B:
        r = B
        B = A % B
        A = r
    return r

def mcd_R(n,m):##mcd recursivo##
    if(m==0):
        return n
    else:
        r = n%m
    return mcd_R(m,r)

def mcm_I(n,m):##mcm iterativo##
    A = max(n,m)
    while A % n != 0 or A%m!= 0:
        A = A+1
    return A



def mcm_F(n,m):##mcm no hay recursivo, si formula##
    r = n*m/mcd_R(n,m)
    return r

n = int(input("numero n: "))
m = int(input("numero m: "))

print("Por mcd iterativo de",n,",",m,"es: ",mcd_I(n,m))
print("Por mcd recursivo de",n,",",m,"es: ",mcd_R(n,m))
print("Por mcm iterativo de",n,",",m,"es: ",mcm_I(n,m))
print("Por mcm recursivo de",n,",",m,"es: ",mcm_F(n,m))


