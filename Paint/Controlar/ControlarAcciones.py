from random import randint
from Modelo.figuras.Circulo import Circulo
from Modelo.figuras.Cuadrilatero import Cuadrilatero

class ControlarAcciones:

    def crearCirculo(self, lienzo):
        c= Circulo(randint(0,400),randint(0,300), 2, 'blue', randint(0,150))
        c.dibujar(lienzo)

    def crearCuadrilatero(self, lienzo):
        c= Cuadrilatero(randint(0,400),randint(0,300), 2, 'red', randint(0,200), randint(0, 150))
        c.dibujar(lienzo)
