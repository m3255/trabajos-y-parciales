from tkinter import *
from tkinter import ttk

from Controlar.ControlarAcciones import ControlarAcciones
class Ventana:

    def __init__(self, titulo):

        ca=ControlarAcciones()
        self.ventana = Tk()
        self.ventana.resizable(0,0)
        self.ventana.geometry('400x375')
        self.ventana.configure(bg='AliceBlue')
        self.ventana.title(titulo)
        self.canvas = Canvas(self.ventana, width=400,height=300, bg='white')
        self.canvas.pack()
        ttk.Button(self.ventana, text='Salir', command=self.ventana.destroy).pack(side=BOTTOM)
        ttk.Button(self.ventana, text='Circulo', command=lambda:ca.crearCirculo(self.canvas)).pack(side=TOP)
        ttk.Button(self.ventana, text='Cuadrilatero', command=lambda: ca.crearCuadrilatero(self.canvas)).pack(side=TOP)