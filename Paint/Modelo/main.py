##@author
##Andres Felipe Wilches Torres - 20172020114
##Sergio Sebastian Ramirez Santamaria - 20172020142
##
from graficos.Ventana import Ventana
from Modelo.figuras.Cuadrilatero import Cuadrilatero
from Modelo.figuras.Circulo import Circulo
global ventana

if __name__ == '__main__':
    ventana = Ventana('MiniPaint')

    ventana.ventana.mainloop()