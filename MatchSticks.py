####@authors
#### Andres Felipe Wilches Torres - 20172020114
#### Sergio Sebastian Ramirez Santamaria - 20172020142
####
def minimo(s1,s2):
    if(len(s1)==0):
        return s2
    if(len(s1)!=len(s2)):
        if(len(s1)<len(s2)):
            return s1
        else:
            return s2
    if (s1 < s2):
        return s1
    else:
        return s2

def maximo (s1,s2):
    if (len(s1) == 0):
        return s2
    if (len(s1) != len(s2)):
        if (len(s1) > len(s2)):
            return s1
        else:
            return s2
    if (s1 > s2):
        return s1
    else:
        return s2

mathsticks = [6,2,5,5,4,5,6,3,7,6]
dp1 = {}
dp2 = {}

for g in range(0,100):
    dp1 [g] = ""
    dp2 [g] = ""

for i in range (2,101):
    a = ""
    b = ""
    temp = 0
    s = -1
    for j in range(0, 10):
        temp = i - mathsticks[j]
        s = s + 1
        if (temp == 0 and j!= 0):
            a = minimo(a,str(s))
            b = maximo(b,str(s))
        if (temp >= 2):
            a = minimo(a,dp1[temp] + str(s))
            b = maximo(b,dp2[temp] + str(s))
    dp1[i] = a
    dp2[i] = b
t = 1
while (t != 0):
    n = int(input("Ingrese la cantidad de palitos: "))
    print(dp1[n],"\n")
    print(dp2[n],"\n")
    t = int(input("¿Desea volver a repetir el programa?, si es asi digite un numero distinto de 0"))